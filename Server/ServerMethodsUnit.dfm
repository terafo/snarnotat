object ServerMethods: TServerMethods
  OldCreateOrder = False
  Height = 393
  Width = 723
  object FDCRestServer: TFDConnection
    Params.Strings = (
      'Database=C:\TestDB\MobileNotes\MOBILENOTES.IB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Server=127.0.0.1'
      'DriverID=IB')
    FormatOptions.AssignedValues = [fvDataSnapCompatibility]
    FormatOptions.DataSnapCompatibility = True
    LoginPrompt = False
    Left = 96
    Top = 336
  end
  object FDQGetEmployees: TFDQuery
    Connection = FDCRestServer
    Left = 648
    Top = 312
  end
  object FDQGetNotes: TFDQuery
    Connection = FDCRestServer
    Left = 656
    Top = 80
  end
  object FDQCustomers: TFDQuery
    Connection = FDCRestServer
    SQL.Strings = (
      'select * from CUSTOMER')
    Left = 648
    Top = 256
  end
  object FDQPostNotes: TFDQuery
    Connection = FDCRestServer
    Left = 568
    Top = 80
  end
  object GenNotesID: TFDQuery
    Connection = FDCRestServer
    Left = 24
    Top = 336
  end
  object FDQuery1: TFDQuery
    Connection = FDCRestServer
    SQL.Strings = (
      'select NOTEID, TITLE, PRIORITY, CUSTOMER.NAME '
      '                             from NOTES '
      
        '                             left join CUSTOMER on NOTES.CUSTID ' +
        '= CUSTOMER.CUSTOID '
      '                             where STATUS = 10'
      '                             order by PRIORITY;')
    Left = 536
    Top = 240
    object FDQuery1NOTEID: TIntegerField
      FieldName = 'NOTEID'
      Origin = 'NOTEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQuery1TITLE: TStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      Required = True
      Size = 50
    end
    object FDQuery1PRIORITY: TIntegerField
      FieldName = 'PRIORITY'
      Origin = 'PRIORITY'
    end
    object FDQuery1NAME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
    object FDQuery1RIGHTS: TSmallintField
      FieldName = 'RIGHTS'
      Origin = 'RIGHTS'
    end
    object FDQuery1DETAILS: TStringField
      FieldName = 'DETAILS'
      Origin = 'DETAILS'
      Size = 100
    end
    object FDQuery1DATE_CREATED: TSQLTimeStampField
      FieldName = 'DATE_CREATED'
      Origin = 'DATE_CREATED'
    end
    object FDQuery1DATE_FINISHED: TSQLTimeStampField
      FieldName = 'DATE_FINISHED'
      Origin = 'DATE_FINISHED'
    end
  end
  object FDQCloseNotes: TFDQuery
    Connection = FDCRestServer
    SQL.Strings = (
      'update NOTES'
      'set NOTECLOSED = :NCLOSED'
      'where NOTEID = :NID')
    Left = 480
    Top = 80
    ParamData = <
      item
        Name = 'NCLOSED'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end
      item
        Name = 'NID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 0
      end>
  end
  object FDQUpdateNotes: TFDQuery
    Connection = FDCRestServer
    Left = 384
    Top = 80
  end
end
