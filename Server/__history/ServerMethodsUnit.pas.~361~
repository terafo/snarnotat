unit ServerMethodsUnit;

interface

uses System.SysUtils, System.Classes, System.Json, Datasnap.DSHTTPWebBroker,
  Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.IB, FireDAC.Phys.IBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI, FireDAC.Stan.StorageJSON,
  FireDAC.Stan.StorageBin, Data.FireDACJSONReflect, Web.HTTPApp, Datasnap.DSHTTPCommon;

type
{$METHODINFO ON}
  TServerMethods = class(TDataModule)
    FDCRestServer: TFDConnection;
    FDQGetEmployees: TFDQuery;
    FDQGetNotes: TFDQuery;
    FDQCustomers: TFDQuery;
    FDQPostNotes: TFDQuery;
    GenNotesID: TFDQuery;
    FDQuery1: TFDQuery;
    FDQuery1NOTEID: TIntegerField;
    FDQuery1TITLE: TStringField;
    FDQuery1PRIORITY: TIntegerField;
    FDQuery1NAME: TStringField;
    FDQuery1RIGHTS: TSmallintField;
    FDQuery1DETAILS: TStringField;
    FDQuery1DATE_CREATED: TSQLTimeStampField;
    FDQuery1DATE_FINISHED: TSQLTimeStampField;
    FDQCloseNotes: TFDQuery;
  private
    { Private declarations }
    function GetGenID(GeneratorNavn: String): Integer;
  public
    { Public declarations }
//  Employee Functions
    procedure getEmployees(Request: TWebRequest; Response: TWebResponse);
//    procedure postEmployees(Request: TWebRequest; Response: TWebResponse );

//  Notes Functions
    procedure getNotes(Request: TWebRequest; Response: TWebResponse);
    procedure postNotes(Request: TWebRequest; Response: TWebResponse);
    procedure closeNotes(Request: TWebRequest; Response: TWebResponse);
  end;
{$METHODINFO OFF}

var
  MethodsUnit: TServerMethods;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses ServerForm;

{$R *.dfm}

// -----------------------------------------------------------------------------
// EMPLOYEE FUNCTIONS
procedure TServerMethods.getEmployees(Request: TWebRequest; Response: TWebResponse);
var
  a: TJSONArray;
  o: TJSONObject;
begin
  FDQGetEmployees.SQL.Text := 'select * from EMPLOYEES;';

  if FDQGetEmployees.Active = False then
    FDQGetEmployees.Active := True;
  if FDQGetEmployees.Active then begin
    if FDQGetEmployees.RecordCount>0 then
      a := TJSONArray.Create;
    try
      FDQGetEmployees.First;
      while (not FDQGetEmployees.Eof) do begin
        o := TJSONObject.Create;
        o.AddPair('EmployeeID', TJSONNumber.Create( FDQGetEmployees.FieldByName('EMPLID').AsInteger));
        o.AddPair('Name', FDQGetEmployees.FieldByName('NAME').AsString);
        o.AddPair('Password', FDQGetEmployees.FieldByName('PASSWORDS').AsString);
        o.AddPair('Rights', TJSONNumber.Create( FDQGetEmployees.FieldByName('RIGHTS').AsInteger));
        a.AddElement(o);
        FDQGetEmployees.Next;
      end;
    finally
      Response.ContentType := 'application/json';
      Response.Content := a.ToString;
      a.DisposeOf;
    end;
  end;
end;
// -----------------------------------------------------------------------------
// NOTE ID GENERATOR
function TServerMethods.GetGenID(GeneratorNavn: String): Integer;
begin
  with GenNotesID do begin
    SQL.Clear;
    SQL.ADD('SELECT GEN_ID (' + GeneratorNavn + ', 1) from rdb$database');
    Open;

    // Fields 0 er fyrsta felt � Queryini.
    Result := Fields[0].AsInteger;
  end;
end;
// -----------------------------------------------------------------------------
// NOTES FUNCTIONS
// GET NOTES LIST
procedure TServerMethods.getNotes(Request: TWebRequest; Response: TWebResponse);
var
  a: TJSONArray;
  o: TJSONObject;
begin
  if Request.QueryFields.Count>0 then begin
    FDQGetNotes.SQL.Text := ' select NOTEID, TITLE, PRIORITY, CUSTOMER.NAME, ' +
                        ' RIGHTS, DETAILS, DATE_CREATED, DATE_FINISHED ' +
                        ' from NOTES ' +
                        ' left join CUSTOMER on NOTES.CUSTID = CUSTOMER.CUSTOID ' +
                        ' where NOTEID = :NID and STATUS = 10;';
    FDQGetNotes.Params.ParamByName('NID').AsString := Request.QueryFields.Values['NoteID'];
  end
  else
    FDQGetNotes.SQL.Text := ' select NOTEID, TITLE, PRIORITY, CUSTOMER.NAME ' +
                            ' from NOTES ' +
                            ' left join CUSTOMER on NOTES.CUSTID = CUSTOMER.CUSTOID ' +
                            ' where STATUS = 10 ' +
                            ' order by PRIORITY; ';

  FDQGetNotes.Active := True;
  if FDQGetNotes.Active then begin
    if FDQGetNotes.RecordCount>0 then
      a := TJSONArray.Create;
    try
      FDQGetNotes.First;
      while (not FDQGetNotes.Eof) do begin
        o := TJSONObject.Create;
        o.AddPair('NID', TJSONNumber.Create( FDQGetNotes.FieldByName('NID').AsInteger));
        o.AddPair('Title', FDQGetNotes.FieldByName('TITLE').AsString);
        o.AddPair('Priority', TJSONNumber.Create( FDQGetNotes.FieldByName('PRIORITY').AsInteger));
        o.AddPair('CustomerName', FDQGetNotes.FieldByName('NAME').AsString);
        //If there is a parameter, add these values
        if Request.QueryFields.Count>0 then begin
        o.AddPair('Rights', TJSONNumber.Create( FDQGetNotes.FieldByName('RIGHTS').AsInteger));
        o.AddPair('Details', FDQGetNotes.FieldByName('DETAILS').AsString);
        o.AddPair('DateCreated', FDQGetNotes.FieldByName('DATE_CREATED').AsString);
        o.AddPair('DateFinished', FDQGetNotes.FieldByName('DATE_FINISHED').AsString);
        end;
        a.AddElement(o);
        FDQGetNotes.Next;
      end;
    finally
      Response.ContentType := 'application/json';
      Response.Content := a.ToString;
      a.DisposeOf;
    end;
  end;

end;
// POST NOTE
procedure TServerMethods.postNotes(Request: TWebRequest;
  Response: TWebResponse);
var
  RequestObject: TJSONObject;
  a: TJSONArray;
  today: TDateTime;
  NewRecordID: Integer;
begin
  // today := Now;

  RequestObject := TJSONObject.ParseJSONValue(Request.Content) as TJSONObject;

  // Insert new record
  FDQPostNotes.SQL.Text :=  ' insert into NOTES ( TITLE, PRIORITY,  ' +
                            ' RIGHTS, DETAILS, CREATEDBY, DATAAREAID, CUSTID, ' +
                            ' DATE_CREATED, NOTEID ) ' +
                            ' VALUES ( :TITLE, :PRIORITY, ' +
                            ' :RIGHTS, :DETAILS, :CREATEDBY, :DATAAREAID, :CUSTID, ' +
                            ' :DATE_CREATED, :NOTEID ); ';
  // Set query parameters

  FDQPostNotes.Params.ParamByName('TITLE').AsString := RequestObject.GetValue('Title').Value;
  FDQPostNotes.Params.ParamByName('PRIORITY').AsInteger := StrToInt( RequestObject.GetValue('Priority').Value );
  FDQPostNotes.Params.ParamByName('RIGHTS').AsSmallInt := StrToInt( RequestObject.GetValue('Rights').Value );
  FDQPostNotes.Params.ParamByName('DETAILS').AsString := RequestObject.GetValue('Details').Value;
  FDQPostNotes.Params.ParamByName('CUSTID').AsInteger := StrToInt( RequestObject.GetValue('CustID').Value );
  FDQPostNotes.Params.ParamByName('CREATEDBY').AsString := RequestObject.GetValue('CreatedBy').Value;
  FDQPostNotes.Params.ParamByName('DATAAREAID').AsSmallInt := 1;
  FDQPostNotes.Params.ParamByName('DATE_CREATED').AsDateTime := Now;




  // Set the ID last
  NewRecordID := GetGenID('GEN_NOTEID');
  FDQPostNotes.Params.ParamByName('NOTEID').AsInteger := NewRecordID;

  // Execute SQL
  FDQPostNotes.ExecSQL;

//  RequestObject.AddPair('NOTEID', TJSONNumber.Create(NewRecordID));
//  a := TJSONArray.Create;
//
//  try
//    a.AddElement(RequestObject);
//    Response.ContentType := 'application/json';
//    Response.Content := a.ToString;
//  finally
//    a.DisposeOf;
//  end;
end;
// DELETE NOTE
procedure TServerMethods.closeNotes(Request: TWebRequest;
  Response: TWebResponse);
var
  RequestObject: TJSONObject;
  NoteID: Integer;
  NoteClosed: Integer;
begin

  // Store Parameters from REST Request in variables
  NoteID := StrToInt( Request.QueryFields.Values['NoteID'] );
  NoteClosed := StrToInt( Request.QueryFields.Values['NoteClosed'] );
  // Insert SQL text into FDQuery
  FDQCloseNotes.SQL.Text := ' update NOTES ' +
                            ' set NOTECLOSED = :NOTECLOSED, DATE_FINISHED = :DATEFINISHED ' +
                            ' where NOTEID = :NID; ';

  // Set the parameters of the query
  FDQCloseNotes.Params.ParamByName('NID').AsInteger := NoteID;
  FDQCloseNotes.Params.ParamByName('NOTECLOSED').AsInteger := NoteClosed;

  // Set DateFinished if Note is Closed
  if NoteClosed = 1 then
    FDQCloseNotes.Params.ParamByName('DATEFINISHED').AsDateTime := Now
  // Remove DateFinished if Note is not Closed
  else
    FDQCloseNotes.Params.ParamByName('DATEFINISHED').Clear();
  // Execute Query
  FDQCloseNotes.ExecSQL;

end;


end.

