unit ServerMethodsUnit;

interface

uses System.SysUtils, System.Classes, System.Json, Datasnap.DSHTTPWebBroker,
  Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.IB, FireDAC.Phys.IBDef,
  FireDAC.Phys.IBBase, FireDAC.Comp.UI, FireDAC.Stan.StorageJSON,
  FireDAC.Stan.StorageBin, Data.FireDACJSONReflect, Web.HTTPApp, Datasnap.DSHTTPCommon;

type
{$METHODINFO ON}
  TServerMethods = class(TDataModule)
    FDCRestServer: TFDConnection;
    FDQGetEmployees: TFDQuery;
    FDQGetNotes: TFDQuery;
    FDQCustomers: TFDQuery;
    FDQPostNotes: TFDQuery;
    GenNotesID: TFDQuery;
    FDQuery1: TFDQuery;
  private
    { Private declarations }
    function GetGenID(GeneratorNavn: String): Integer;
  public
    { Public declarations }
    procedure getEmployees(Request: TWebRequest; Response: TWebResponse);
    procedure getNotes(Request: TWebRequest; Response: TWebResponse);
//    procedure postEmployees(Request: TWebRequest; Response: TWebResponse );
    procedure postNotes(Request: TWebRequest; Response: TWebResponse );
  end;
{$METHODINFO OFF}

var
  MethodsUnit: TServerMethods;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses ServerForm;

{$R *.dfm}

procedure TServerMethods.getEmployees(Request: TWebRequest; Response: TWebResponse);
var
  a: TJSONArray;
  o: TJSONObject;
begin
  FDQGetEmployees.SQL.Text := 'select * from employees;';

  FDQGetEmployees.Active := True;
  if FDQGetEmployees.Active then begin
    if FDQGetEmployees.RecordCount>0 then
      a := TJSONArray.Create;
    try
      FDQGetEmployees.First;
      while (not FDQGetEmployees.Eof) do begin
        o := TJSONObject.Create;
        o.AddPair('EmployeeID', TJSONNumber.Create( FDQGetEmployees.FieldByName('EMPLID').AsInteger));
        o.AddPair('Name', FDQGetEmployees.FieldByName('NAME').AsString);
        o.AddPair('Password', FDQGetEmployees.FieldByName('PASSWORDS').AsString);
        o.AddPair('Rights', TJSONNumber.Create( FDQGetEmployees.FieldByName('RIGHTS').AsInteger));
        a.AddElement(o);
        FDQGetEmployees.Next;
      end;
    finally
      Response.ContentType := 'application/json';
      Response.Content := a.ToString;
      a.DisposeOf;
    end;
  end;
end;

function TServerMethods.GetGenID(GeneratorNavn: String): Integer;
begin
  with GenNotesID do begin
    SQL.Clear;
    SQL.ADD('SELECT GEN_ID (' + GeneratorNavn + ', 1) from rdb$database');
    Open;

    // Fields 0 er fyrsta felt � Queryini.
    Result := Fields[0].AsInteger;
  end;
end;

procedure TServerMethods.getNotes(Request: TWebRequest; Response: TWebResponse);
var
  a: TJSONArray;
  o: TJSONObject;
begin
  if Request.QueryFields.Count>0 then begin
    FDQGetNotes.SQL.Text := ' select NOTEID, TITLE, PRIORITY, CUSTOMER.NAME, ' +
                        ' RIGHTS, DETAILS, DATE_CREATED, DATE_FINISHED ' +
                        ' from NOTES ' +
                        ' left join CUSTOMER on NOTES.CUSTID = CUSTOMER.CUSTOID ' +
                        ' where NOTEID = :NID; ';
    FDQGetNotes.Params.ParamByName('NID').AsString := Request.QueryFields.Values['NID'];
  end
  else
    FDQGetNotes.SQL.Text := 'select NOTEID, TITLE, PRIORITY, CUSTOMER.NAME ' +
                            ' from NOTES ' +
                            ' left join CUSTOMER on NOTES.CUSTID = CUSTOMER.CUSTOID ' +
                            ' order by PRIORITY; ';

  FDQGetNotes.Active := True;
  if FDQGetNotes.Active then begin
    if FDQGetNotes.RecordCount>0 then
      a := TJSONArray.Create;
    try
      FDQGetNotes.First;
      while (not FDQGetNotes.Eof) do begin
        o := TJSONObject.Create;
        o.AddPair('NoteID', TJSONNumber.Create( FDQGetNotes.FieldByName('NOTEID').AsInteger));
        o.AddPair('Title', FDQGetNotes.FieldByName('TITLE').AsString);
        o.AddPair('Priority', TJSONNumber.Create( FDQGetNotes.FieldByName('PRIORITY').AsInteger));
        o.AddPair('CustomerID', FDQGetNotes.FieldByName('CUSTOMER.NAME').AsString);
        //If there is a parameter, add these values
        if Request.QueryFields.Count>0 then begin
        o.AddPair('Rights', TJSONNumber.Create( FDQGetNotes.FieldByName('RIGHTS').AsInteger));
        o.AddPair('Details', FDQGetNotes.FieldByName('DETAILS').AsString);
        o.AddPair('DateFinish', FDQGetNotes.FieldByName('DATE_FINISHED').AsString);
        o.AddPair('DateCreate', FDQGetNotes.FieldByName('DATE_CREATED').AsString);
        end;
        a.AddElement(o);
        FDQGetNotes.Next;
      end;
    finally
      Response.ContentType := 'application/json';
      Response.Content := a.ToString;
      a.DisposeOf;
    end;
  end;

end;

procedure TServerMethods.postNotes(Request: TWebRequest;
  Response: TWebResponse);
var
  RequestObject: TJSONObject;
  a: TJSONArray;
  today: TDateTime;
begin
  //today := Now;

  RequestObject := TJSONObject.ParseJSONValue(Request.Content) as TJSONObject;

  // Insert new record
  FDQPostNotes.SQL.Text :=  ' insert into NOTES ( TITLE, PRIORITY,  ' +
                            ' RIGHTS, DETAILS, CREATEDBY, DATAAREAID, NOTEID ) ' +
                            ' VALUES ( :TITLE, :PRIORITY, ' +
                            ' :RIGHTS, :DETAILS, :CREATEDBY, :DATAAREAID, :NOTEID ); ';
  // Set query parameters

  FDQPostNotes.Params.ParamByName('TITLE').AsString := RequestObject.GetValue('Title').Value;
  FDQPostNotes.Params.ParamByName('PRIORITY').AsInteger := StrToInt( RequestObject.GetValue('Priority').Value );
  FDQPostNotes.Params.ParamByName('RIGHTS').AsSmallInt := StrToInt( RequestObject.GetValue('Rights').Value );
  FDQPostNotes.Params.ParamByName('DETAILS').AsString := RequestObject.GetValue('Details').Value;
  FDQPostNotes.Params.ParamByName('CREATEDBY').AsString := 'fsdfsd';
  FDQPostNotes.Params.ParamByName('DATAAREAID').AsSmallInt := 1;


//  FDQPostNotes.Params.ParamByName('DATE_CREATED').AsDateTime := today;
//  FDQPostNotes.Params.ParamByName('DATE_FINISHED').AsDateTime := StrToDateTime( '00-00-0000 00:00' );


  // Set the ID last
  FDQPostNotes.Params.ParamByName('NOTEID').AsInteger := GetGenID('GEN_NOTEID');

  // Execute SQL
  FDQPostNotes.ExecSQL;

  a := TJSONArray.Create;
  try
    a.AddElement(RequestObject);
    Response.ContentType := 'application/json';
    Response.Content := a.ToString;
  finally
    a.DisposeOf;
  end;
end;

//procedure TServerMethods.postEmployees(Request: TWebRequest;
//  Response: TWebResponse);
//var
//  RequestObject: TJSONObject;
//  a: TJSONArray;
//begin
//  RequestObject := TJSONObject.ParseJSONValue(Request.Content) as TJSONObject;
//  FDQPostEmployees.SQL.Text :=  ' insert into EMPLOYEES (EMPLID, NAME, PASSWORDS, RIGHTS) ' +
//                                ' VALUES (:EMPLID, :NAME, :PASSWORDS, :RIGHTS); ';
//
//  FDQPostEmployees.Params.ParamByName('EMPLID').AsInteger := GetGenID('GEN_EMPLID');//StrToInt ( RequestObject.GetValue('employeeID').Value) ;
//  FDQPostEmployees.Params.ParamByName('NAME').AsString := RequestObject.GetValue('EmplName').Value;
//  FDQPostEmployees.Params.ParamByName('PASSWORDS').AsString := RequestObject.GetValue('EmplPass').Value;
//  FDQPostEmployees.Params.ParamByName('RIGHTS').AsInteger := StrToInt( RequestObject.GetValue('EmplRights').Value );
//
//
//
//  FDQPostEmployees.ExecSQL;
//
//  a := TJSONArray.Create;
//  try
//    a.AddElement(RequestObject);
//    Response.ContentType := 'application/json';
//    Response.Content := a.ToString;
//  finally
//    a.DisposeOf;
//  end;
//end;

end.

