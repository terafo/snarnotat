unit TabbedFormwithNavigation;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.TabControl, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.Gestures, System.Actions, FMX.ActnList, FMX.Edit, FMX.Objects,
  System.Rtti, FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, dmRestUnit,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,
  FireDAC.UI.Intf, FireDAC.FMXUI.Wait, FireDAC.Stan.Intf, FireDAC.Comp.UI,
  FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON, System.JSON,
  FMX.DateTimeCtrls, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.Layouts, FMX.ListBox, FMX.ListView,
  FMX.ExtCtrls, FMX.Menus, FMX.Memo;

type
  TTabbedwithNavigationForm = class(TForm)
    TabControlMain: TTabControl;
    tabLogin: TTabItem;
    TabControlLogin: TTabControl;
    tabLoginScreen: TTabItem;
    tBarLogin: TToolBar;
    lblLoginToolbar: TLabel;
    btnNext: TSpeedButton;
    tabEmployees: TTabItem;
    tBarEmployees: TToolBar;
    lblEmployeesToolbar: TLabel;
    btnBack: TSpeedButton;
    tabNotes: TTabItem;
    tabSettings: TTabItem;
    ToolBar4: TToolBar;
    lblTitle4: TLabel;
    tabCreateNotes: TTabItem;
    ToolBar5: TToolBar;
    lblTitle5: TLabel;
    GestureManager1: TGestureManager;
    ActionList1: TActionList;
    NextLoginTab: TNextTabAction;
    PreviousLoginTab: TPreviousTabAction;
    pnlLogin: TPanel;
    btnLogin: TButton;
    editUsername: TEdit;
    editPassword: TEdit;
    lblSaveUsername: TLabel;
    swSaveUsername: TSwitch;
    loginLogo: TImage;
    TabControlNotes: TTabControl;
    tBarNoteList: TToolBar;
    tabNotesList: TTabItem;
    tabNotesDetails: TTabItem;
    tBarDetails: TToolBar;
    lblDetailToolbar: TLabel;
    sbtnNoteLists: TSpeedButton;
    NextNoteTab: TNextTabAction;
    PreviousNoteTab: TPreviousTabAction;
    lblNoteListToolbar: TLabel;
    pnlSaveUsername: TPanel;
    BindingsList1: TBindingsList;
    PnlNoteDetails: TPanel;
    pnlNoteIDRightsPrior: TPanel;
    editNotePriority: TEdit;
    editNoteRights: TEdit;
    editNoteID: TEdit;
    lblNoteID: TLabel;
    lblNoteRights: TLabel;
    lblNotePriority: TLabel;
    pnlNoteDates: TPanel;
    teditDateCreated: TTimeEdit;
    teditDateFinished: TTimeEdit;
    lblDateCreated: TLabel;
    lblDateFinished: TLabel;
    editNoteCustName: TEdit;
    lblCustName: TLabel;
    memoNotesDetails: TMemo;
    lblNotesDetails: TLabel;
    PnlCreateNotes: TPanel;
    pnlCrNoteID: TPanel;
    editCrNotePriority: TEdit;
    editCrNoteRights: TEdit;
    lblCrNoteRights: TLabel;
    lblCrNotePriority: TLabel;
    editCrNoteCustomer: TEdit;
    lblCrNoteCustomer: TLabel;
    memoCrNoteDetails: TMemo;
    lblCrNoteDetails: TLabel;
    lblCrNoteMsgRecipient: TLabel;
    editCrNoteMsgRecipient: TEdit;
    ListViewNotes: TListView;
    LinkFillControlToField5: TLinkFillControlToField;
    sBtnCreateNote: TSpeedButton;
    pnlCrNoteTitle: TPanel;
    lblCrNoteTitle: TLabel;
    editCrNoteTitle: TEdit;
    lblCloseRecord: TLabel;
    swCloseRecord: TSwitch;
    BindSourceDB1: TBindSourceDB;
    BindSourceDB2: TBindSourceDB;
    gridEmployee: TStringGrid;
    LinkGridToDataSourceBindSourceDB22: TLinkGridToDataSource;
    procedure GestureDone(Sender: TObject; const EventInfo: TGestureEventInfo;
        var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
        Shift: TShiftState);
    procedure btnLoginClick(Sender: TObject);
    procedure sbtnNoteListsClick(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
    procedure sBtnCreateNotesClick(Sender: TObject);
    procedure ListViewNotesItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure editPasswordKeyUp(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
    procedure tabNotesClick(Sender: TObject);
    procedure swCloseRecordClick(Sender: TObject);
  private
    fJSONArray: TJSONArray;
    fCurrentIndex: int32;
  private
    { Private declarations }
    procedure lockForm(level: Integer);
    procedure openForm(userLevel: Integer);
    procedure loadNotesList(requestPar: Integer);
    procedure showNotesDetails();
    procedure closeNote(nID: String; closedNote: String);
  public
    { Public declarations }
  end;

var
  TabbedwithNavigationForm: TTabbedwithNavigationForm;

implementation
uses
  REST.Types;

{$R *.fmx}
{$R *.iPhone4in.fmx IOS}
{$R *.iPhone55in.fmx IOS}
// -----------------------------------------------------------------------------
// FORM CREATE
procedure TTabbedwithNavigationForm.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  TabControlMain.ActiveTab := tabLogin;
  TabControlLogin.ActiveTab := tabLoginScreen;
  TabControlNotes.ActiveTab := tabNotesList;

   Load the employees table
  with dmRest do begin
    RESTRequestEmployees.Resource := 'Employees';
    RESTRequestEmployees.Method := TRESTRequestMethod.rmGET;
    RESTRequestEmployees.Response := RESTResponseEmployees;

    RESTRequestEmployees.Execute;
  end;

  // Lock forms
  lockForm(1);
end;

// -----------------------------------------------------------------------------
// FORMS
procedure TTabbedwithNavigationForm.lockForm(level: Integer);
begin
  // Disable tabs
  tabNotes.Enabled := False;
  tabSettings.Enabled := False;
  tabCreateNotes.Enabled := False;
end;

procedure TTabbedwithNavigationForm.openForm(userLevel: Integer);
begin

  case userLevel of
    1: begin
    tabSettings.Enabled := True;
    tabCreateNotes.Enabled := True;
    tabNotes.Enabled := True;
    end;
    2: begin
    tabCreateNotes.Enabled := True;
    tabNotes.Enabled := True;
    end;
    3: tabNotes.Enabled := True;
  end;

  // Disable login panel
  pnlLogin.Enabled := False;
end;

// -----------------------------------------------------------------------------
// AUTO CREATED GIBBERISH
procedure TTabbedwithNavigationForm.FormKeyUp(Sender: TObject; var Key: Word;
    var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkHardwareBack then
  begin
    if (TabControlMain.ActiveTab = tabNotesList) then
    begin
      TabControlLogin.Previous;
      Key := 0;
    end;
  end;
end;

procedure TTabbedwithNavigationForm.GestureDone(Sender: TObject;
    const EventInfo: TGestureEventInfo; var Handled: Boolean);
begin
  case EventInfo.GestureID of
    sgiLeft:
      begin
        if TabControlMain.ActiveTab <> TabControlMain.Tabs[TabControlMain.TabCount - 1] then
          TabControlMain.ActiveTab := TabControlMain.Tabs[TabControlMain.TabIndex + 1];
        Handled := True;
      end;

    sgiRight:
      begin
        if TabControlMain.ActiveTab <> TabControlMain.Tabs[0] then
          TabControlMain.ActiveTab := TabControlMain.Tabs[TabControlMain.TabIndex - 1];
        Handled := True;
      end;
  end;
end;

// -----------------------------------------------------------------------------
// Login tab
procedure TTabbedwithNavigationForm.btnLoginClick(Sender: TObject);
var
  userString: string;
  passString: string;
begin
  //Store the edit.text in variables
  userString := editUsername.Text;
  passString := editPassword.Text;

//  // If no username is inserted, alert the user
//  if userString.IsEmpty then begin
//    editUsername.TextPrompt := 'Skriva Br�karanavn her';
//  end
//  // If no password is inserted, alert the user
//  else if passString.IsEmpty then begin
//    editPassword.TextPrompt := 'Skriva Loynior� her';
//  end
//  // If username and password are inserted
//  // Check if they match with the memtable
//  else begin
//    with dmRest do begin
//      FDMemEmployees.First;
//      while not FDMemEmployees.Eof do begin
//        if userString = FDMemEmployees.FieldByName('Name').Value then begin
//          if passString = FDMemEmployees.FieldByName('Password').Value then begin
//            // Enable the other tabs, and clear login info
//            openForm(FDMemEmployees.FieldByName('Rights').Value);
//            loadNotesList(0);
//            TabControlMain.ActiveTab := tabNotes;
//            editUsername.Text := '';
//            editPassword.Text := '';
//
//            // If username and password match, then go to last record of memtable,
//            // to break the while loop
//            FDMemEmployees.Last;
//          end;
//        end;
//        FDMemEmployees.Next;
//      end;
//    end;
//  end;
  loadNotesList(0);
  openForm(dmRest.FDMemEmployees.FieldByName('Rights').Value);
  TabControlMain.ActiveTab := tabNotes;
end;

procedure TTabbedwithNavigationForm.editPasswordKeyUp(Sender: TObject;
  var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
    if (Key = vkReturn) then
       btnLoginClick(nil);
end;

// -----------------------------------------------------------------------------
// Notes tab
// -----------------------------------------------------------------------------
// NOTES TAB FUNCTIONS
procedure TTabbedwithNavigationForm.loadNotesList(requestPar: Integer);
begin
  // Load the Note List
  with dmRest do begin
    RESTRequestNotes.Resource := 'Notes';
    RESTRequestNotes.Method := TRESTRequestMethod.rmGET;
    RESTRequestNotes.Response := RESTResponseNotes;
    if Assigned(RESTRequestNotes.Params) then
      RESTRequestNotes.Params.Clear;
    RESTNotesAdapter.Dataset := FDMemNotes;
    // Create parameter to show more details about current note
    if requestPar>0 then begin
      RESTRequestNotes.AddParameter(
      { Param.name}         'NoteID',
      { Param.value }       requestPar.ToString,
      { Param.kind }        TRESTRequestParameterKind.pkGETorPOST );
      RESTNotesAdapter.Dataset := FDMemNotesDetails;
    end;
    RESTRequestNotes.Execute;

    if requestPar>0 then begin
      if assigned(fJSONArray) then
        fJSONArray.DisposeOf;

      fJSONArray := TJSONObject.ParseJSONValue(RESTResponseNotes.Content) as TJSONArray;
      showNotesDetails;
    end;
  end;
end;

procedure TTabbedwithNavigationForm.closeNote(nID: String; closedNote: String);
begin
  with dmRest do begin
      RESTRequestNotes.Resource := 'notes';
      RESTRequestNotes.Method := TRESTRequestMethod.rmDELETE;
      RESTRequestNotes.Response := RESTResponseNotes;


      RESTRequestNotes.AddParameter(
      { Param.name}         'NoteID',
      { Param.value }       nID,
      { Param.kind }        TRESTRequestParameterKind.pkGETorPOST );
      RESTRequestNotes.AddParameter(
      { Param.name}         'NoteClosed',
      { Param.value }       closedNote,
      { Param.kind }        TRESTRequestParameterKind.pkGETorPOST );
      RESTNotesAdapter.Dataset := FDMemCloseNotes;

      RestRequestNotes.Execute;
    end;
end;

procedure TTabbedwithNavigationForm.showNotesDetails;
var
  o: TJSONObject;
begin
  if fJSONArray.Count>0 then begin
    o := fJSONArray.Items[0] as TJSONObject;
    editNoteID.Text := o.GetValue('NoteID').Value;
    lblDetailToolbar.Text := o.GetValue('Title').Value;
    editNoteRights.Text := o.GetValue('Rights').Value;
    editNotePriority.Text := o.GetValue('Priority').Value;
    editNoteCustName.Text := o.GetValue('CustomerName').Value;

    if Assigned( o.GetValue('DateCreated') ) then
      teditDateCreated.DateTime := StrToDateTime( o.GetValue('DateCreated').Value )
    else
      teditDateCreated.isEmpty := True;
    if Assigned( o.GetValue('DateFinished') ) then
      teditDateFinished.DateTime := StrToDateTime( o.GetValue('DateFinished').Value )
    else
      teditDateFinished.IsEmpty := True;
    memoNotesDetails.Text := o.GetValue('Details').Value;
  end;
end;

procedure TTabbedwithNavigationForm.ListViewNotesItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin

    loadNotesList(strToInt(AItem.Detail));
    TabControlNotes.ActiveTab:=tabNotesDetails;

end;

// -----------------------------------------------------------------------------
// NOTES TAB CLICKS (BUTTON SWITCHES)
procedure TTabbedwithNavigationForm.sbtnNoteListsClick(Sender: TObject);
begin

  if swCloseRecord.IsChecked then
    swCloseRecord.IsChecked := False;

  loadNotesList(0);
  TabControlNotes.ActiveTab := tabNotesList;
end;

procedure TTabbedwithNavigationForm.swCloseRecordClick(Sender: TObject);
begin
  if swCloseRecord.IsChecked then
    closeNote(editNoteID.Text, '1')
//    ShowMessage('Checked')
  else if not swCloseRecord.IsChecked then
    closeNote( editNoteID.Text, '0' );
//    ShowMessage('Not Checked');
end;

procedure TTabbedwithNavigationForm.tabNotesClick(Sender: TObject);
begin
  loadNotesList(0);
end;

// -----------------------------------------------------------------------------
// CREATE-NOTES TAB
procedure TTabbedwithNavigationForm.sBtnCreateNotesClick(Sender: TObject);
var
  userString: String;
begin
  with dmRest do begin
    RESTRequestNotes.Resource := 'notes';
    RESTRequestNotes.Method :=  TRESTRequestMethod.rmPOST;
    RESTRequestNotes.Response := RESTResponseNotes;

    userString := FDMemEmployees.FieldByName('Name').Value;

    RESTRequestNotes.Body.ClearBody;
    with RESTRequestNotes.Body.JSONWriter do begin
      WriteStartObject;
      WritePropertyName('Title');
      WriteValue( editCrNoteTitle.Text );
      WritePropertyName('Priority');
      WriteValue( editCrNotePriority.Text );
      WritePropertyName('Rights');
      WriteValue( editCrNoteRights.Text );
      WritePropertyName('Details');
      WriteValue( memoCrNoteDetails.Text );
      WritePropertyName('Customer');
      WriteValue( editCrNoteCustomer.Text );
      WritePropertyName('CreatedBy');
      WriteValue( userString );
      WritePropertyName('MsgRecipient');
      WriteValue( editCrNoteMsgRecipient.Text );
      WriteEnd;
    end;
    RESTNotesAdapter.Dataset := FDMemPostNotes;

    RESTRequestNotes.Execute;
  end;
end;

end.

