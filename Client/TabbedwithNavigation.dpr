program TabbedWithNavigation;

uses
  System.StartUpCopy,
  FMX.Forms,
  TabbedFormwithNavigation in 'TabbedFormwithNavigation.pas' {TabbedwithNavigationForm},
  dmRestUnit in 'dmRestUnit.pas' {dmRest: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmRest, dmRest);
  Application.CreateForm(TTabbedwithNavigationForm, TabbedwithNavigationForm);
  Application.Run;
end.
