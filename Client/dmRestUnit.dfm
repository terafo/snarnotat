object dmRest: TdmRest
  OldCreateOrder = False
  Height = 449
  Width = 874
  object RESTClient: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'UTF-8, *;q=0.8'
    BaseURL = 'http://localhost:8080'
    Params = <>
    HandleRedirects = True
    Left = 824
    Top = 400
  end
  object RESTRequestEmployees: TRESTRequest
    Client = RESTClient
    Params = <>
    Resource = 'Employees'
    Response = RESTResponseEmployees
    SynchronizedEvents = False
    Left = 144
    Top = 8
  end
  object RESTResponseEmployees: TRESTResponse
    ContentType = 'application/json'
    Left = 48
    Top = 24
  end
  object RESTEmployeesAdapter: TRESTResponseDataSetAdapter
    Active = True
    Dataset = FDMemEmployees
    FieldDefs = <>
    ResponseJSON = RESTResponseEmployees
    Left = 144
    Top = 64
  end
  object FDMemEmployees: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'EmployeeID'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Name'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Password'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Rights'
        DataType = ftWideString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 48
    Top = 80
  end
  object RESTRequestNotes: TRESTRequest
    Client = RESTClient
    Params = <>
    Resource = 'Notes'
    Response = RESTResponseNotes
    SynchronizedEvents = False
    Left = 448
    Top = 8
  end
  object RESTResponseNotes: TRESTResponse
    ContentType = 'application/json'
    Left = 368
    Top = 24
  end
  object RESTNotesAdapter: TRESTResponseDataSetAdapter
    Active = True
    Dataset = FDMemNotes
    FieldDefs = <>
    ResponseJSON = RESTResponseNotes
    Left = 448
    Top = 64
  end
  object FDMemNotes: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'NoteID'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Title'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Priority'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'CustomerName'
        DataType = ftWideString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 368
    Top = 80
    object FDMemNotesNoteID: TWideStringField
      FieldName = 'NoteID'
      Size = 255
    end
    object FDMemNotesTitle: TWideStringField
      FieldName = 'Title'
      Size = 255
    end
    object FDMemNotesPriority: TWideStringField
      FieldName = 'Priority'
      Size = 255
    end
    object FDMemNotesCustomerName: TWideStringField
      FieldName = 'CustomerName'
      Size = 255
    end
  end
  object FDMemNotesDetails: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'NoteID'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Title'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Priority'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Rights'
        DataType = ftWideString
        Size = 255
      end
      item
        Name = 'Details'
        DataType = ftWideString
        Size = 255
      end>
    CachedUpdates = True
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 368
    Top = 136
    object FDMemNotesDetailsNoteID: TWideStringField
      FieldName = 'NoteID'
      Size = 255
    end
    object FDMemNotesDetailsTitle: TWideStringField
      FieldName = 'Title'
      Size = 255
    end
    object FDMemNotesDetailsPriority: TWideStringField
      FieldName = 'Priority'
      Size = 255
    end
    object FDMemNotesDetailsRights: TWideStringField
      FieldName = 'Rights'
      Size = 255
    end
    object FDMemNotesDetailsDetails: TWideStringField
      FieldName = 'Details'
      Size = 255
    end
  end
  object FDMemPostNotes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 368
    Top = 192
  end
  object FDMemCloseNotes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 368
    Top = 248
  end
  object FDMemUpdateNotes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 368
    Top = 304
  end
end
